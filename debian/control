Source: oddjob
Section: admin
Priority: optional
Maintainer: Debian FreeIPA Team <pkg-freeipa-devel@lists.alioth.debian.org>
Uploaders: Timo Aaltonen <tjaalton@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dbus,
 libdbus-1-dev,
 libkrb5-dev,
 libpam-dev,
 libsasl2-dev,
 libselinux1-dev,
 libxml2-dev,
 pkg-config,
 systemd,
 systemd-dev,
 xmlto,
Standards-Version: 4.6.1
Homepage: https://pagure.io/oddjob/
Vcs-Git: https://salsa.debian.org/freeipa-team/oddjob.git
Vcs-Browser: https://salsa.debian.org/freeipa-team/oddjob

Package: oddjob
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 dbus,
 systemd,
Description: D-Bus service which runs odd jobs -- daemon
 Oddjob is a D-Bus service which performs particular tasks for clients which
 connect to it and issue requests using the system-wide message bus.
 .
 This package contains the oddjob daemon.

Package: oddjob-mkhomedir
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 oddjob (= ${binary:Version}),
Description: Oddjob helper which creates and populates home directories
 Oddjob is a D-Bus service which performs particular tasks for clients which
 connect to it and issue requests using the system-wide message bus.
 .
 This package contains the oddjob helper which can be used by the
 pam_oddjob_mkhomedir module to create a home directory for a user
 at login-time.
